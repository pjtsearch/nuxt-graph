import Vue from "vue";
import Vuetify from "vuetify/lib";
import colors from "vuetify/es5/util/colors";
import "@mdi/font/css/materialdesignicons.css";

Vue.use(Vuetify, {
  theme: {
    primary: colors.deepPurple.accent4,
    secondary: colors.indigo.base,
    accent: colors.blue.base,
    error: colors.red.base,
    warning: colors.orange.base,
    info: colors.teal.base,
    success: colors.green.base
  },
  iconfont: "mdi"
});
