class Regexes {
  static n = new RegExp(/(?:[0-9]*\.[0-9]+|[0-9]+)/).source;
  static x = new RegExp(/x/).source;
  static o = new RegExp(/{[^+\-\/\(\)]+}/).source;
  static s = new RegExp(/\//).source;
  static a = new RegExp(/\*/).source;
  static p = new RegExp(/\+/).source;
  static m = new RegExp(/\-/).source;
  static pow = new RegExp(/\^/).source;
  static esc = new RegExp(/[^+\-\/\(\)]*/).source;
  static sp = new RegExp(/\(/).source;
  static ep = new RegExp(/\)/).source;
  constructor() {}
}
export default Regexes;
