import R from "./Regexes.class";

class Parse {
  eq: string;
  constructor({ eq }: { eq: string }) {
    this.eq = eq;
  }
  private parseTemplate({ eq, regex, cb }: { eq: string; regex: RegExp; cb: Function }) {
    var stack: any[] = [];
    var i: number = 0;
    var newEq: string = eq;
    var matches: any[] = [...newEq["matchAll"](regex)];
    while (matches.length > 0) {
      matches.forEach(item => {
        stack.push(cb({ item, stack }));
      });
      stack.forEach(item => {
        newEq = newEq.replace(regex, JSON.stringify(item));
        matches = [...newEq["matchAll"](regex)];
      });
      stack = [];
      if (i > 5) {
        console.log("break in template");
        break;
      }
      i++;
    }
    return newEq;
  }
  private parseOneLevel({ eq }: { eq: string }) {
    var newEq = eq;

    // (...)
    // newEq = this.parseTemplate({
    //   eq: newEq,
    //   regex: new RegExp(`${R.sp}(?:${R.o}|${R.n}|${R.x})${R.ep}`),
    //   cb: ({ item, stack }) => {
    //     // console.log(item);
    //     item = this.parseIfJSON(item[0].replace(/\(/, "").replace(/\)/, ""));
    //     // console.log(item);
    //     return { op: "parenthesis", args: [item] };
    //   }
    // });

    newEq = this.parseTemplate({
      eq: newEq,
      regex: new RegExp(`${R.sp}(?:[^${R.ep}${R.sp}]+)${R.ep}`),
      cb: ({ item, stack }) => {
        // console.log(item);
        if (this.isJsonString(item[0].replace(/\(/, "").replace(/\)/, ""))) {
          item = this.parseIfJSON(item[0].replace(/\(/, "").replace(/\)/, ""));
        } else {
          item = this.parseIfJSON(
            this.parseOneLevel({
              eq: item[0].replace(/\(/, "").replace(/\)/, "")
            })
          );
        }
        // console.log(item);
        return { op: "parenthesis", args: [item] };
      }
    });

    //x^2
    newEq = this.parseTemplate({
      eq: newEq,
      regex: new RegExp(`(?:${R.n}|${R.x}|${R.o})${R.pow}(?:${R.n}|${R.x}|${R.o})+${R.esc}`),
      cb: ({ item, stack }) => {
        var base: number | any = this.parseIfJSON(item[0].split("^")[0]);
        var exponent: number | any = this.parseIfJSON(item[0].split("^")[1]);
        return { op: "exponent", args: [base, exponent] };
      }
    });

    //5x
    newEq = this.parseTemplate({
      eq: newEq,
      regex: new RegExp(`${R.n}${R.x}`),
      cb: ({ item, stack }) => {
        var coef: number | any = this.parseIfJSON(item[0].match(new RegExp(`${R.n}`))[0]);
        return { op: "multiply", args: [coef, { op: "number", args: ["x"] }] };
      }
    });

    // MAKE ABLE TO DO MULTIPLE OBJS
    //5{...}
    newEq = this.parseTemplate({
      eq: newEq,
      regex: new RegExp(`${R.n}${R.o}`),
      cb: ({ item, stack }) => {
        var coef: number | any = this.parseIfJSON(item[0].match(new RegExp(`${R.n}`))[0]);
        var mult: number | any = this.parseIfJSON(item[0].match(new RegExp(`${R.o}`))[0]);
        return { op: "multiply", args: [coef, mult] };
      }
    });

    //{...}{...}
    newEq = this.parseTemplate({
      eq: newEq,
      regex: new RegExp(`(${R.o}){2,}`),
      cb: ({ item, stack }) => {
        // console.log(item);
        // console.log("item", item[0].split(/(\}\{)/).splice(1,1));
        var items: any[] = item[0]
          .split(/(\}\{)/)
          .filter(i => i !== "}{")
          .map(i => {
            var r = i;
            if (i.charAt(0) !== "{") r = "{" + r;
            if (i.charAt(i.length - 1) !== "}") r = r + "}";
            return r;
          })
          .map(i => this.parseIfJSON(i));
        // console.log("items", [...items]);
        return { op: "multiply", args: items };
      }
    });

    // // /3
    // newEq = this.parseTemplate({
    //   eq: newEq,
    //   regex: /\/(?:{[^+\-\/\(\)]+}|\d+)/,
    //   cb: ({ item, stack }) => {
    //     // var multipliers = item[0].split("*");
    //     // multipliers = multipliers.map(item => {
    //     //   // try {
    //     //   //   return JSON.parse(item);
    //     //   // } catch (err) {
    //     //   //   return item;
    //     //   // }
    //     //   return this.parseIfJSON(item);
    //     // });
    //     // return { op: "multiply", args: multipliers };
    //     item = this.parseIfJSON(item[0].replace("/", ""));
    //     return { op: "oneOver", args: [item] };
    //   }
    // });

    //x*x
    // newEq = this.parseTemplate({
    //   eq: newEq,
    //   regex: /(?:{[^+\-\/\(\)]+}\*|\d+\*|x\*)+(?:{[^+\-\/\(\)]+}|\d+|x)/,
    //   cb: ({ item, stack }) => {
    //     var multipliers = item[0].split("*");
    //     multipliers = multipliers.map(item => {
    //       // try {
    //       //   return JSON.parse(item);
    //       // } catch (err) {
    //       //   return item;
    //       // }
    //       return this.parseIfJSON(item);
    //     });
    //     return { op: "multiply", args: multipliers };
    //   }
    // });

    //x*x/x
    newEq = this.parseTemplate({
      eq: newEq,
      regex: new RegExp(`(?:${R.o}${R.a}|${R.n}${R.a}|${R.x}${R.a}|${R.o}${R.s}|${R.n}${R.s}|${R.x}${R.s})+(?:${R.o}|${R.n}|${R.x})`),
      cb: ({ item, stack }) => {
        var multipliers = item[0]
          .split(new RegExp(`(${R.a}|${R.s})`))
          .filter(item => item !== "*")
          .map((item, index, array) => {
            if (array[index - 1] === "/") {
              return { op: "oneOver", args: [this.parseIfJSON(item)] };
            } else {
              return item;
            }
          })
          .filter(item => item !== "/")
          .map(item => {
            return this.parseIfJSON(item);
          });
        return { op: "multiply", args: multipliers };
      }
    });

    //
    //
    //
    //
    //
    // MAKE MERGED DIVIDE AND MULTIPLY
    // current prototype
    // [..."5+2/2*5".matchAll(/(?:{[^+\-\/\(\)]+}\*|\d+\*|{[^+\-\/\(\)]+}\/|\d+\/)+(?:{[^+\-\/\(\)]+}|\d+)/)][0][0]
    // .split(/(\/|\*)/)
    // .map((item,index,array)=>{
    //
    // })
    //
    //
    //

    //x+x
    // newEq = this.parseTemplate({
    //   eq: newEq,
    //   regex: /(?:{.+}\+|\d+\+)+(?:{.+}|\d+)/,
    //   cb: ({ item, stack }) => {
    //     var addends = item[0].split("+");
    //     addends = addends.map(item => {
    //       try {
    //         return JSON.parse(item);
    //       } catch (err) {
    //         return item;
    //       }
    //     });
    //     return { op: "add", args: addends };
    //   }
    // });
    //x-x
    // newEq = this.parseTemplate({
    //   eq: newEq,
    //   regex: /(?:{.+}\-|\d+\-)+(?:{.+}|\d+)/,
    //   cb: ({ item, stack }) => {
    //     var subtractors = item[0].split("-");
    //     subtractors = subtractors.map(item => {
    //       try {
    //         return JSON.parse(item);
    //       } catch (err) {
    //         return item;
    //       }
    //     });
    //     return { op: "subtract", args: subtractors };
    //   }
    // });

    //x+x-x
    newEq = this.parseTemplate({
      eq: newEq,
      regex: new RegExp(`(?:${R.o}${R.p}|${R.n}${R.p}|${R.x}${R.p}|${R.o}${R.m}|${R.n}${R.m}|${R.x}${R.m})+(?:${R.o}|${R.n}|${R.x})`),
      cb: ({ item, stack }) => {
        // var addends = item[0].split("+");
        var addends = item[0]
          .split(new RegExp(`(${R.p}|${R.m})`))
          .filter(item => item !== "+")
          .map((item, index, array) => {
            if (array[index - 1] === "-") {
              return { op: "negative", args: [this.parseIfJSON(item)] };
            } else {
              return item;
            }
          })
          .filter(item => item !== "-")
          .map(item => {
            // try {
            //   return JSON.parse(item);
            // } catch (err) {
            //   return item;
            // }
            return this.parseIfJSON(item);
          });
        return { op: "add", args: addends };
      }
    });
    return newEq;
  }
  private isJsonString(str: string) {
    try {
      JSON.parse(str);
    } catch (e) {
      return false;
    }
    return true;
  }
  private parseIfJSON(str: string) {
    // try {
    //   return JSON.parse(str);
    // } catch (err) {
    //   return { op: "number", args: [str] };
    // }

    // @ts-ignore
    if (isNaN(str) && str !== "x") {
      try {
        return JSON.parse(str);
      } catch (err) {
        return str;
      }
    } else {
      return { op: "number", args: [str] };
    }
  }
  parseWhole() {
    var eq = this.eq;
    var oldEq = eq;
    var i = 0;
    while (!this.isJsonString(eq)) {
      // console.log(eq);
      var oldEq = eq;
      eq = this.parseOneLevel({ eq });
      if (i > 100) {
        console.log("break");
        break;
      }
      if (eq === oldEq) {
        console.log("break1", eq);
        break;
      }
      i++;
    }
    try {
      return JSON.parse(eq);
    } catch (err) {
      return eq;
    }
  }
  get answer() {
    return this.parseWhole();
  }
}

export default Parse;
