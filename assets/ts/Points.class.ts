import Solve from "./Solve.class";

class Points {
  objEq: any;
  xs: number;
  xe: number;
  constructor({ objEq, xs, xe }) {
    this.objEq = objEq;
    this.xs = xs;
    this.xe = xe;
  }
  get answer() {
    var x: number = this.xs;
    var res = [];
    while (x <= this.xe) {
      // @ts-ignore
      res.push([x, new Solve({ objEq: this.objEq, x }).answer]);
      x++;
    }
    return res;
  }
}

export default Points;
