import deepcopy from "deepcopy";
class Solve {
  x: number;
  objEq: object;
  objEqSolved: any;
  constructor({ objEq, x }) {
    this.objEq = objEq;
    this.x = x;
  }
  private replaceX() {
    var objEqClone: any = deepcopy(this.objEq);
    var iterate = obj => {
      if (obj.op !== "number") {
        obj.args.forEach((item, index) => {
          if (item.answer === undefined) {
            // 				obj.args[index].found = true
            // 				console.log(obj)
            // 				wholeIndex.push(item.op ? item.op : "number:" + index)
            iterate(item);
          } else {
          }
        });
      } else {
        if (obj.args[0] === "x") {
          obj.answer = this.x;
        } else {
          obj.answer = Number(obj.args[0]);
        }
        // 		wholeIndex.push("f")
        // 		console.log(wholeIndex)
        // 		test1.push(obj)
      }
    };
    iterate(objEqClone);
    return objEqClone;
  }
  solve() {
    //
    //
    //ZERO IS FALSE
    //
    //
    this.objEqSolved = this.replaceX();
    var objEqClone: any = deepcopy(this.objEqSolved);
    var iterate = obj => {
      // if (obj.args) {
      obj.args
        .filter(item => item.answer === undefined)
        .forEach((item, index) => {
          if (
            /*item.args && */ item.args.filter(o => o.answer === undefined)
              .length === 0
          ) {
            answerItem(item);
          }
          iterate(item);
        });
      // }
    };
    var answerItem = item => {
      let answer;
      switch (item.op) {
        case "parenthesis":
          answer = item.args[0].answer;
          item.answer = answer;
          break;
        case "exponent":
          answer = Math.pow(item.args[0].answer, item.args[1].answer);
          item.answer = answer;
          break;
        case "multiply":
          answer = item.args.reduce(
            (accumulator, value) => accumulator * value.answer,
            1
          );
          item.answer = answer;
          break;
        case "oneOver":
          answer = 1 / item.args[0].answer;
          item.answer = answer;
          break;
        case "negative":
          answer = -1 * item.args[0].answer;
          item.answer = answer;
          break;
        case "add":
          answer = item.args.reduce(
            (accumulator, value) => accumulator + value.answer,
            0
          );
          item.answer = answer;
          break;
      }
    };
    var iterateIndex = 0;
    var oldObjEqClone = objEqClone;
    while (
      objEqClone.args.filter(item => item.answer === undefined).length !== 0
    ) {
      oldObjEqClone = objEqClone;
      console.log("run");
      iterate(objEqClone);
      if (iterateIndex > 999) break;
      iterateIndex++;
    }
    answerItem(objEqClone);
    // this.answer = objEqClone.answer;
    return objEqClone.answer;
  }
  get answer() {
    return this.solve();
  }
}

export default Solve;
