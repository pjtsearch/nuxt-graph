importScripts('/graphing-calculator/_nuxt/workbox.4c4f5ca6.js')

workbox.precaching.precacheAndRoute([
  {
    "url": "/graphing-calculator/_nuxt/0ef5c8c1f03979b07969.js",
    "revision": "58d8b50d60dfa97f9ecd1370af039757"
  },
  {
    "url": "/graphing-calculator/_nuxt/30ec737be4d9da1664d7.js",
    "revision": "f3f51632d282f2776bf7b700eb6426f2"
  },
  {
    "url": "/graphing-calculator/_nuxt/37af76353ac02fa2be29.js",
    "revision": "8622ce2b94bd6a86a3e05f47d3ee9c0d"
  },
  {
    "url": "/graphing-calculator/_nuxt/3ddfad2260d820a2c164.js",
    "revision": "eae251ffc661a8001154e3bc163a0dab"
  },
  {
    "url": "/graphing-calculator/_nuxt/4764f70224f810824406.js",
    "revision": "bb4f2fc5d469335bac4cd8bcd47835bf"
  },
  {
    "url": "/graphing-calculator/_nuxt/905fbe27bc7b794789dc.js",
    "revision": "7a29d4cb35a234e5ca7d1837828dee68"
  },
  {
    "url": "/graphing-calculator/_nuxt/a1021dfd870cf5a3b36d.js",
    "revision": "53744ff10eed307ff5b52a2e9bdd1737"
  }
], {
  "cacheId": "nuxt-graph",
  "directoryIndex": "/",
  "cleanUrls": false
})

workbox.clientsClaim()
workbox.skipWaiting()

workbox.routing.registerRoute(new RegExp('/graphing-calculator/_nuxt/.*'), workbox.strategies.cacheFirst({}), 'GET')

workbox.routing.registerRoute(new RegExp('/graphing-calculator/.*'), workbox.strategies.networkFirst({}), 'GET')
